import { Loadable } from 'utils/components'
import { FAVORITES_PATH as path } from 'constants/paths'

export default {
  path,
  component: Loadable({
    loader: () =>
      import(/* webpackChunkName: 'Favorites' */ './components/FavoritesPage')
  })
}
