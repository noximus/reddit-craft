import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles'
import styles from './FavoritesPage.styles'

export default compose(
  // create listener for favorites, results go into redux
  firestoreConnect([{ collection: 'favorites' }]),
  // map redux state to props
  connect(({ firestore: { data } }) => ({
    favorites: data.favorites
  })),
  withStyles(styles)
)
