import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { LOGIN_PATH } from 'constants/paths'

export const FavoritesPage = ({ favorites, classes, auth }) => (
  <div className={classes.container}>
    {!auth.uid ? <Redirect to={LOGIN_PATH} /> : null}
    <span>Favorites Page Component</span>
    <pre>{JSON.stringify(favorites, null, 2)}</pre>
  </div>
)

FavoritesPage.propTypes = {
  classes: PropTypes.object.isRequired, // from enhancer (withStyles)
  favorites: PropTypes.object, // from enhancer (firestoreConnect + connect)
  auth: PropTypes.object
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  }
}

export default connect(mapStateToProps)(FavoritesPage)
