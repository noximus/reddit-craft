import FavoritesPage from './FavoritesPage'
import enhance from './FavoritesPage.enhancer'

export default enhance(FavoritesPage)
