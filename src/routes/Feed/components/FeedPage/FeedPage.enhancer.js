import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { withStyles } from '@material-ui/core/styles'
import styles from './FeedPage.styles'

export default compose(
  // create listener for feed, results go into redux
  firestoreConnect([{ collection: 'feed' }]),
  // map redux state to props
  connect(({ firestore: { data } }) => ({
    feed: data.feed
  })),
  withStyles(styles)
)
