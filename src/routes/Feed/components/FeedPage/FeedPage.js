import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { LOGIN_PATH } from 'constants/paths'

export const FeedPage = ({ feed, classes, auth }) => (
  <div className={classes.container}>
    {/* {console.log(auth.uid)} */}
    {!auth.uid ? <Redirect to={LOGIN_PATH} /> : null}
    <span>Reddit feed here</span>
    <pre>{JSON.stringify(feed, null, 2)}</pre>
  </div>
)

FeedPage.propTypes = {
  classes: PropTypes.object.isRequired, // from enhancer (withStyles)
  feed: PropTypes.object // from enhancer (firestoreConnect + connect)
}

const mapStateToProps = state => {
  return {
    auth: state.firebase.auth
  }
}

export default connect(mapStateToProps)(FeedPage)
