import FeedPage from './FeedPage'
import enhance from './FeedPage.enhancer'

export default enhance(FeedPage)
