import { Loadable } from 'utils/components'
import { FEED_PATH as path } from 'constants/paths'

export default {
  path,
  component: Loadable({
    loader: () =>
      import(/* webpackChunkName: 'Feed' */ './components/FeedPage')
  })
}
