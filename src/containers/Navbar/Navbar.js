import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import InputBase from '@material-ui/core/InputBase'
import Badge from '@material-ui/core/Badge'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import { fade } from '@material-ui/core/styles/colorManipulator'
import { withStyles } from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu'
import Button from '@material-ui/core/Button'
import SearchIcon from '@material-ui/icons/Search'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MailIcon from '@material-ui/icons/Mail'
import NotificationsIcon from '@material-ui/icons/Notifications'
import MoreIcon from '@material-ui/icons/MoreVert'
import { FEED_PATH, FAVORITES_PATH, LIST_PATH } from 'constants/paths'
import AccountMenu from './AccountMenu'
import LoginMenu from './LoginMenu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// export const Navbar = ({
//   avatarUrl,
//   displayName,
//   authExists,
//   goToAccount,
//   handleLogout,
//   closeAccountMenu,
//   anchorEl,
//   handleMenu,
//   classes
// }) => (
//   <AppBar position="static">
//     <Toolbar>
//     <IconButton className={classes.menuButton} color="inherit" aria-label="Open drawer"><MenuIcon /></IconButton>
//     <div className={classes.leftNav}>
//       <Button className={classes.title} color="inherit" component={Link} to={authExists ? FEED_PATH : '/feed'}>G/r/analog </Button>
//       <Button className={classes.title} color="inherit" component={Link} to={authExists ? FAVORITES_PATH : '/'}>favorites(0) </Button>
//     </div>
//       {authExists ? (
//         <AccountMenu
//           avatarUrl={avatarUrl}
//           displayName={displayName}
//           onLogoutClick={handleLogout}
//           goToAccount={goToAccount}
//           closeAccountMenu={closeAccountMenu}
//           handleMenu={handleMenu}
//           anchorEl={anchorEl}
//         />
//       ) : (
//         <LoginMenu />
//       )}
//     </Toolbar>
//   </AppBar>
// )

class Navbar extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    avatarUrl: null,
    displayName: null,
    authExists: null,
    goToAccount: null,
    handleLogout: null,
    closeAccountMenu: null,
    handleMenu: null,
    classes: null
  }

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleMenuClose = () => {
    this.setState({ anchorEl: null })
    this.handleMobileMenuClose()
  }

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget })
  }

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null })
  }

  render() {
    const { anchorEl, mobileMoreAnchorEl } = this.state
    const { classes } = this.props
    const isMenuOpen = Boolean(anchorEl)
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}>
        <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={this.handleMenuClose}>My account</MenuItem>
      </Menu>
    )

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}>
        <MenuItem onClick={this.handleProfileMenuOpen}>
          <IconButton color="inherit">
            <AccountCircle />
          </IconButton>
          <p>Profile</p>
        </MenuItem>
      </Menu>
    )

    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar className={classes.toolBar}>
            <Button
              className={classes.navBtn}
              color="inherit"
              component={Link}
              to={FEED_PATH}>
              <FontAwesomeIcon icon={['fab', 'reddit-alien']} /> /r/analog
            </Button>
            <Button
              className={classes.navBtnInv}
              color="inherit"
              component={Link}
              to={FAVORITES_PATH}>
              <FontAwesomeIcon icon="heart" />
              &nbsp;favorites (0)
            </Button>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon className={classes.searchIconSvg} />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
              />
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              {/* {console.log('hello',this.props.authExists)} */}
              {this.props.authExists ? (
                <AccountMenu
                  avatarUrl={this.props.avatarUrl}
                  displayName={this.props.displayName}
                  onLogoutClick={this.props.handleLogout}
                  goToAccount={this.props.goToAccount}
                  closeAccountMenu={this.props.closeAccountMenu}
                  handleMenu={this.props.handleMenu}
                  anchorEl={this.props.anchorEl}
                />
              ) : (
                <LoginMenu />
              )}
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                aria-haspopup="true"
                onClick={this.handleMobileMenuOpen}
                color="inherit">
                <MoreIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        {renderMenu}
        {renderMobileMenu}
      </div>
    )
  }
}
Navbar.propTypes = {
  classes: PropTypes.object.isRequired, // from enhancer (withStyles)
  displayName: PropTypes.string, // from enhancer (flattenProps - profile)
  avatarUrl: PropTypes.string, // from enhancer (flattenProps - profile)
  authExists: PropTypes.bool, // from enhancer (withProps - auth)
  goToAccount: PropTypes.func.isRequired, // from enhancer (withHandlers - router)
  handleLogout: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  closeAccountMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  handleMenu: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
  anchorEl: PropTypes.object // from enhancer (withStateHandlers - handleMenu)
}

export default Navbar
