import { fade } from '@material-ui/core/styles/colorManipulator';

export default theme => ({
  root: {
    color: 'green',
  },
  appBar: {
    backgroundColor: 'white',
    boxShadow: 'none',
    borderBottom: '1px solid #ccc',
    marginLeft: 0,
    height: '40px'
  },
  toolBar: {
    backgroundColor: 'white',
    boxShadow: 'none',
    borderBottom: '1px solid #ccc',
    marginLeft: 0,
    minHeight: '41px',
    paddingLeft: '0px',
    paddingRight: '0px'
  },
  leftNav: {
    flexGrow: 1,
    fontSize: '16px',
    color: 'white',
    fontFamily: 'Helvetica'
  },
  navBtn: {
    color: 'white',
    textTransform: 'none',
    fontSize: '16px',
    fontFamily: 'Helvetica',
    backgroundColor: '#57a2e4',
    borderRadius: 0
  },
  navBtnInv: {
    color: '#57a2e4',
    textTransform: 'none',
    fontSize: '16px',
    fontFamily: 'Helvetica',
    backgroundColor: 'white',
    borderRadius: 0,
    '&:hover': {
      color: 'white',
      backgroundColor: '#57a2e4',
    }
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: 0,
    backgroundColor: fade('#57a2e4', 0.15),
    fontFamily: 'Helvetica',
    height: '100%',
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchIconSvg: {
    fill: '#57a2e4'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  }
})
